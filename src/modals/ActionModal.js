import React from "react";
import FormFooter from "../components/Form/FormFooter/FormFooter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ActionModel = () => {
  return (
    <React.Fragment>
      <div
        className={`modal overflow-scroll fixed w-full h-full top-0 left-0 z-50 `}
      >
        <div
          className="overlay min-h-full min-w-full flex items-center justify-center"
          style={{ backgroundColor: "#00000066" }}
        >
          <div className="bg-white rounded-lg shadow-lg w-3/4 xs:2/3 sm:w-1/2 lg:w-1/3">
            <div className="p-4 border-b flex justify-between">
              <h1 className="text-lg">Filter On</h1>

              <div className="text-center px-2 inline-block cursor-pointer text-red-600 hover:text-red-900">
                <FontAwesomeIcon className="text-sm" icon="times" />
              </div>
            </div>
            <form action="" className="p-4">
              <div className="">
                <button className="w-auto h-10 px-4 py-2 leading-6 sm:leading-5 bg-red-500 rounded-lg text-sm font-medium text-white shadow-sm hover:opacity-75 focus:outline-none mr-1">
                  Deactive
                </button>
              </div>
              <FormFooter className="">
                <button
                  type="button"
                  className="w-auto h-10 px-4 py-2 leading-6 sm:leading-5 bg-white rounded-lg border border-gray-400 text-sm font-medium text-gray-700 shadow-sm hover:opacity-75 focus:outline-none mr-1"
                >
                  Cancel
                </button>
              </FormFooter>
            </form>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default ActionModel;

import React from "react";
import FormFooter from "../components/Form/FormFooter/FormFooter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import FormControl from "../components/Form/FormControl/FormControl";
import Input from "../components/Form/Input/Input";
import Button from "../components/Form/Button/Button";
import axios from 'axios';

const DesignationSetupModal = ({setModalOpenClose,inputData ,setInputData , updateCreate , handleUpdateDataSetup,activeDeactive,setActiveDeactive,updateError,setUpdateError}) => {

    const handleTypeCreate = e =>{
        e.preventDefault();
        if(updateCreate) 
        {
            handleUpdateDataSetup();
        }
        else
        {
          const designationData = {name: inputData,is_active:"true"}
        axios.post("https://api.picoehr.com/api/v1/designations",designationData).then(
          res=>{console.log(res);setModalOpenClose(false);setInputData('');setActiveDeactive(!activeDeactive)}
        ).catch(err => console.log(err))
        }
        console.log(inputData)
      }

  return (
    <React.Fragment>
      <div
        className={`modal overflow-scroll fixed w-full h-full top-0 left-0 z-50 `}
      >
        <div
          className="overlay min-h-full min-w-full flex items-center justify-center"
          style={{ backgroundColor: "#00000066" }}
        >
          <div className="bg-white rounded-lg shadow-lg w-3/4 xs:2/3 sm:w-1/2 md:w-1/3 lg:w-1/4">
            <div className="p-4 border-b flex justify-between">
              <h1 className="text-lg">Create New Designation</h1>

              <div className="text-center px-2 inline-block cursor-pointer text-red-600 hover:text-red-900">
                <FontAwesomeIcon className="text-sm" icon="times" onClick = {()=>{setModalOpenClose(false);setInputData('');setUpdateError('')}} />
              </div>
            </div>
            <form action="" className="px-4">
            <p style={{color:'red'}}>{updateError}</p>
              <FormControl>
                <Input className="w-full" type="text" labelText="Designation Name"
                    onChange ={(e)=>setInputData(e.target.value)}
                    value = {inputData} />
              </FormControl>
              <FormFooter className="pb-4">
                <button
                  type="button"
                  className="w-auto h-10 px-4 py-2 leading-6 sm:leading-5 bg-white rounded-lg border border-gray-400 text-sm font-medium text-gray-700 shadow-sm hover:opacity-75 focus:outline-none mr-1" onClick={()=>{setModalOpenClose(false);setInputData('');setUpdateError('')}}
                >
                  Cancel
                </button>
                <Button notClick={(!inputData)? true :false}  className={`${(!inputData)? 'cursor-not-allowed':''}`} onClick ={handleTypeCreate}>{updateCreate ? "Update" : "Create"}</Button>
              </FormFooter>
            </form>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default DesignationSetupModal;

import React from "react";
import FormFooter from "../components/Form/FormFooter/FormFooter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ConfirmModal = ({ handleOnSubmit, icon, text }) => {
  const overlay = {
    backgroundColor: "rgba(0,0,0,0.24)",
  };
  return (
    <div
      style={overlay}
      className={`modal fixed w-full  h-full top-0 left-0 z-50 `}
    >
      <div className="overlay flex w-full h-full items-center justify-center">
        <div className="w-9/12 pb-1 px-3 md:w-2/5 lg:w-1/4 rounded-lg bg-white">
          <div className="w-full flex-col pt-5  flex items-center justify-center">
            <span>
              {/* <i className="far fa-frown text-4xl text-red-500"></i> */}
              <FontAwesomeIcon icon={icon} className="text-3xl text-red-500" />
            </span>
            <span className="text-base md:text-lg text-gray-600 text-center my-3">
              {text}
            </span>

            <FormFooter className="mb-2">
              <button
                type="button"
                className="w-auto px-4 py-2 leading-6 sm:leading-5 bg-white rounded-lg border border-gray-400 text-sm font-medium text-gray-700 shadow-sm hover:opacity-75 focus:outline-none mr-4"
              >
                No
              </button>
              <button
                className="w-auto px-4 py-2 leading-6 sm:leading-5 bg-red-500 rounded-lg border border-gray-400 text-sm font-medium text-white shadow-sm hover:opacity-75 focus:outline-none ml-4"
                type="button"
                onClick={handleOnSubmit}
              >
                Yes
              </button>
            </FormFooter>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConfirmModal;

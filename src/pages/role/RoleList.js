import React, { useState, useEffect } from "react";
import Topbar from "../../components/Topbar/Topbar";
import Input from "../../components/Form/Input/Input";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RoleSetupModal from "../../modals/RoleSetupModal";
import axios from "axios";
import { act } from "react-dom/test-utils";
import LoadingModal from "../../modals/LoadingModal";

const RoleList = ({ toggleSidebar }) => {
  const [modalOpenClose, setModalOpenClose] = useState(false);
  const [roles, setRoles] = useState();
  const [filterData, setFilterData] = useState();
  const [types, setTypes] = useState();
  const [tableToggle, setTableToggle] = useState(false);
  const [checkIndex, setCheckIndex] = useState();
  const [inputData, setInputData] = useState("");
  const [selectData, setSelectData] = useState();
  const [updateCreate, setUpdateCreate] = useState();
  const [updateError, setUpdateError] = useState("");
  const [searchData, setSearchData] = useState("");

  const [activeDeactive, setActiveDeactive] = useState(false);

  useEffect(() => {
    axios
      .get("https://api.picoehr.com/api/v1/roles")
      .then((response) => {
        setRoles(response.data.data);
        setFilterData(response.data.data);
      })
      .catch((err) => console.log(err));
    console.log("tatatatata", tableToggle);
  }, [activeDeactive]);

  useEffect(() => {
    axios
      .get("https://api.picoehr.com/api/v1/types")
      .then((response) => {
        setTypes(response.data.data);
      })
      .catch((err) => console.log(err));
  }, [modalOpenClose]);

  const handleRoleActiveDeactive = (e) => {
    const arr = {
      id: roles[checkIndex - 1].id,
      name: roles[checkIndex - 1].name,
      is_active: roles[checkIndex - 1].is_active === "true" ? "false" : "true",
      type_id: roles[checkIndex].type_id,
    };
    axios
      .put("https://api.picoehr.com/api/v1/roles/" + `${checkIndex}`, arr)
      .then((res) => {
        console.log(res);
        setTableToggle(!tableToggle);
        setActiveDeactive(!activeDeactive);
      })
      .catch((err) => console.log(err));
  };

  const handleUpdateDataSetup = (e) => {
    const arr = {
      id: roles[checkIndex - 1].id,
      name: inputData,
      is_active: roles[checkIndex - 1].is_active,
      type_id: selectData,
    };
    axios
      .put("https://api.picoehr.com/api/v1/roles/" + `${checkIndex}`, arr)
      .then((res) => {
        console.log(res);
        setModalOpenClose(false);
        setActiveDeactive(!activeDeactive);
        setInputData("");
        setUpdateError("");
        setSelectData();
      })
      .catch((err) => {
        console.log(err.response.data.error);
        setUpdateError(err.response.data.error);
      });
  };
  const handleRoleUpdate = (e) => {
    setInputData(roles[checkIndex - 1].name);
    setSelectData(roles[checkIndex - 1].type_id);
    setModalOpenClose(true);
    setTableToggle(false);
    setUpdateCreate(true);
  };

  const handleRoleSearch = (e) => {
    setSearchData(e.target.value);
  };
  let ShowData = searchData
    ? roles.filter((item) => item.name.toLowerCase().includes(searchData))
    : roles;
  return (
    <React.Fragment>
      <Topbar toggleSidebar={toggleSidebar} pageTitle="Role List" />
      {!roles && <LoadingModal />}
      {roles && (
        <div className="my-1 px-6 sm:px-8 py-4">
          <div className="bg-white rounded-lg shadow-lg text-sm mb-4 p-2">
            <form action="" className="flex flex-wrap items-center">
              <div className="w-full md:w-auto flex-grow px-1">
                <Input
                  className="w-full"
                  type="text"
                  placeholder="Search Role"
                  onChange={handleRoleSearch}
                />
              </div>
            </form>
          </div>
          <div className="flex flex-wrap -mx-2">
            {ShowData &&
              ShowData.map((values, index) => {
                //console.log(index , checkIndex ,tableToggle)
                return (
                  <div className="w-full sm:w-1/2 lg:w-1/4 px-2" key={index}>
                    <div className="rounded-lg bg-white shadow-lg text-sm mb-4 p-4 flex items-center justify-between">
                      <div className="flex align-middle">
                        <div
                          className={`w-3 h-3 rounded-full mt-2 ${
                            values.is_active === "true"
                              ? "bg-green-500"
                              : "bg-orange-500"
                          }`}
                        ></div>
                        <div className="text-lg font-bold ml-2">
                          {values.name}
                        </div>
                      </div>
                      <div className="relative">
                        <div
                          className="toggle-table-action"
                          onClick={() => {
                            parseInt(values.id) === checkIndex
                              ? setTableToggle(!tableToggle)
                              : setTableToggle(true);
                            setCheckIndex(parseInt(values.id));
                          }}
                        >
                          <FontAwesomeIcon
                            icon="ellipsis-v"
                            className="text-gray-600 cursor-pointer hover:text-gray-900"
                          />
                        </div>

                        <div
                          className={`bg-white shadow-lg rounded-lg absolute mt-4 top-0 right-0 py-4 text-sm font-bold text-gray-600 z-10 table-action ${
                            !(
                              tableToggle && parseInt(values.id) === checkIndex
                            ) && "hidden"
                          }`}
                        >
                          <div className="py-2 pl-4 pr-6 flex items-center cursor-pointer text-sm font-bold hover:bg-green-200 hover:text-green-600">
                            <div className="text-center flex-none mr-4 w-6">
                              <FontAwesomeIcon
                                className="text-md"
                                icon="pencil-alt"
                              />
                            </div>
                            <span
                              className="flex-none"
                              onClick={handleRoleUpdate}
                            >
                              Update
                            </span>
                          </div>
                          <div className="py-2 pl-4 pr-6 flex items-center cursor-pointer text-sm font-bold hover:bg-red-100 hover:text-red-600">
                            <div className="text-center flex-none mr-4 w-6">
                              <FontAwesomeIcon
                                className="text-md"
                                icon={
                                  values.is_active === "true"
                                    ? "trash-alt"
                                    : "recycle"
                                }
                              />
                            </div>
                            <span
                              className="flex-none"
                              onClick={handleRoleActiveDeactive}
                            >
                              {values.is_active === "true"
                                ? "Deactive"
                                : "Active"}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
          </div>
        </div>
      )}

      <div
        className="toggle-modal w-12 h-12 bg-green-500 text-white rounded-full shadow-lg fixed bottom-0 right-0 mb-4  mr-4 flex items-center justify-center cursor-pointer hover:bg-green-700"
        onClick={() => {
          setModalOpenClose(true);
          setUpdateCreate(false);
        }}
      >
        <FontAwesomeIcon className="text-sm" icon="plus" />
      </div>
      {modalOpenClose && (
        <RoleSetupModal
          updateError={updateError}
          setUpdateError={setUpdateError}
          setModalOpenClose={setModalOpenClose}
          types={types}
          inputData={inputData}
          setInputData={setInputData}
          selectData={selectData}
          setSelectData={setSelectData}
          updateCreate={updateCreate}
          activeDeactive={activeDeactive}
          setActiveDeactive={setActiveDeactive}
          handleUpdateDataSetup={handleUpdateDataSetup}
        />
      )}
    </React.Fragment>
  );
};

export default RoleList;

import React, { useState, useEffect } from "react";
import Topbar from "../../components/Topbar/Topbar";
import Input from "../../components/Form/Input/Input";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import FormWithSection from "../../components/Form/FormWithSection/FormWithSection";
import FormSection from "../../components/Form/FormWithSection/FormSection";
import FormControl from "../../components/Form/FormControl/FormControl";
import Select from "../../components/Form/Input/Select";
import CheckBox from "../../components/Form/Input/Checkbox";
import FormFooter from "../../components/Form/FormFooter/FormFooter";
import Button from "../../components/Form/Button/Button";
import axios from "axios";

// [[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false],[false,false,false,false,false]]

const PermissionSetup = ({ toggleSidebar }) => {
  const [modules, setModules] = useState();
  const [roles, setRoles] = useState();
  const [reload, setReload] = useState(false);
  //const [checkData, setCheckData] = useState([]);
  const [apiSuccess, setApiSuccess] = useState("");

  const [selectData, setSelectData] = useState();
  const [checkUncheck, setCheckUncheck] = useState();
  useEffect(() => {
    axios.get("https://api.picoehr.com/api/v1/modules").then((res) => {
      setModules(res.data.data);
    });
    axios.get("https://api.picoehr.com/api/v1/roles").then((res) => {
      setRoles(res.data.data);
    });
  }, []);
  //https://api.picoehr.com/api/v1/roles/2/permissions
  useEffect(() => {
    if (selectData) {
      //setCheckData([])
      axios
        .get(
          "https://api.picoehr.com/api/v1/roles/" +
            `${selectData}` +
            "/permissions"
        )
        .then((res) => {
          //console.log(res.data.data);
          //setCheckData(res.data.data);
          const arr = res.data.data.map((res, index) => {
            return res.id;
          });
          const arrayCheck = modules.map((mod, index) => {
            const perCheck = mod.permissions.map((per, index) => {
              if (arr.includes(per.id)) {
                return true;
              } else return false;
            });
            return perCheck;
          });
          //console.log(arrayCheck[10][2])
          setCheckUncheck(arrayCheck);
          //setCheckData(arr);
        })
        .catch((err) => console.log(err));
    }
      setApiSuccess("");
  }, [selectData]);

  useEffect(() => {}, [reload]);

  const handleCheckUncheckPermission = (e, id, index) => {
    const checkChangeData = checkUncheck;
    checkChangeData[id - 1][index] = !checkChangeData[id - 1][index];
    setCheckUncheck(checkChangeData);
    setReload(!reload);
    console.log(checkChangeData, id);
  };
  const handlePermissionCreate = (e) => {
    e.preventDefault();
    const arrResult = [];
    checkUncheck.map((res, index1) => {
      res.map((tab, index) => {
        if (tab === true) {
          arrResult.push(index1 * 5 + index + 1);
          console.log("true", index1, index);
        }
      });
    });
    console.log(arrResult);
    //const dataput = [36,37,38,39,40];
    if(arrResult.length!==0){
      axios
      .post(
        "https://api.picoehr.com/api/v1/roles/" +
          `${selectData}` +
          "/permissions",
        { permissions: arrResult }
      )
      .then((res) => {
        console.log(res);
        setApiSuccess("Success Update Role Permissions");
        window.scrollTo({ top: 0, behavior: "smooth" });
      })
      .catch((err) => console.log(err));
    }
    else
      {
        setApiSuccess("Please select at least One");
        window.scrollTo({top:0,behavior:"smooth"})
      }
  };
  //console.log(checkData)
  return (
    <React.Fragment>
      <Topbar toggleSidebar={toggleSidebar} pageTitle="Permission Setup" />
      <div className="my-1 px-6 sm:px-8 py-4">
        <form className="bg-white border rounded-lg shadow-md overflow-hidden">
          <FormSection sectionTitle="Create New User Role">
            <p style={{ color: "green" }}>{apiSuccess}</p>
            <FormControl>
              <Select
                className="w-full"
                labelText="User Role"
                name="checkType"
                preViewData="-- Choose One --"
                // optionData={[
                //   { optionValue: "admin", optionText: "Admin" },
                //   { optionValue: "user", optionText: "User" },
                // ]}
                optionData1={roles}
                onChange={(e) => setSelectData(e.target.value)}
                value={selectData}
              />
            </FormControl>
            {modules &&
              modules.map((modulesData, index1) => {
                {
                  /* setCheckUncheck(false); */
                }
                return (
                  <div
                    className="flex items-center flex-wrap py-4 md:py-2 md:px-4 border-b"
                    key={index1}
                  >
                    <h4 className="w-full text-gray-800 text-sm flex-grow mb-2">
                      {modulesData.name}
                    </h4>
                    {modulesData.permissions.map((permissData, index) => {
                      //checkUncheck && console.log(checkUncheck[modulesData.id-1],[index]);
                      return (
                        <CheckBox
                          labelText={permissData.name}
                          key={permissData.name}
                          name={"Permission"}
                          check={
                            //true
                            checkUncheck &&
                            checkUncheck[modulesData.id - 1][index]
                            //(checkData.includes(permissData.id)) && checkUncheck
                          }
                          onChange={(e) =>{checkUncheck && handleCheckUncheckPermission(
                              e,
                              modulesData.id,
                              index
                            )}
                            
                          }
                        />
                      );
                    })}
                  </div>
                );
              })}
            {/* <div className="flex items-center flex-wrap py-4 md:py-2 md:px-4 border-b">
              <h4 className="w-full text-gray-800 text-sm flex-grow mb-2">
                User
              </h4>
              <CheckBox labelText="view" />
              <CheckBox labelText="create" />
              <CheckBox labelText="update" />
              <CheckBox labelText="delete" />
              <CheckBox labelText="print" />
            </div>
            <div className="flex items-center flex-wrap py-4 md:py-2 md:px-4 border-b">
              <h4 className="w-full text-gray-800 text-sm flex-grow mb-2">
                Type
              </h4>
              <CheckBox labelText="view" />
              <CheckBox labelText="create" />
              <CheckBox labelText="update" />
              <CheckBox labelText="delete" />
              <CheckBox labelText="print" />
            </div>
            <div className="flex items-center flex-wrap py-4 md:py-2 md:px-4 border-b">
              <h4 className="w-full text-gray-800 text-sm flex-grow mb-2">
                Role
              </h4>
              <CheckBox labelText="view" />
              <CheckBox labelText="create" />
              <CheckBox labelText="update" />
              <CheckBox labelText="delete" />
              <CheckBox labelText="print" />
            </div> */}
            <FormFooter>
              <Button
                notClick={!selectData ? true : false}
                className={`${!selectData ? "cursor-not-allowed" : ""}`}
                onClick={handlePermissionCreate}
              >
                Create
              </Button>
            </FormFooter>
          </FormSection>
        </form>
      </div>
    </React.Fragment>
  );
};

export default PermissionSetup;

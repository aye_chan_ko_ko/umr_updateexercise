import React, { useEffect ,useState} from "react";
import Topbar from "../../components/Topbar/Topbar";
import Table from "../../components/Table/Table";
import axios from 'axios';
import LoadingModal from "../../modals/LoadingModal";

// function createData(userCode, name, phone, department, address, status) {
//   return [userCode, name, phone, department, address, status];
// }
const tableHeader = [
  "User Code",
  "Name",
  "Phone",
  "Department",
  "Address",
  "Status",
];

function createAction(actionName, actionIcon) {
  return { actionName, actionIcon };
}

const tableAction = [
  createAction("Detail", "eye"),
  createAction("Update", "pencil-alt"),
  createAction("Delete", "trash"),
];

const UserList = ({ toggleSidebar }) => {
  //const line=`${1}`;
  //"https://api.picoehr.com/api/v1/users?page="+line
  const [tableShowData,setTableShowData]=useState();
  useEffect(()=>{axios.get("https://api.picoehr.com/api/v1/users").then(response=>{
    //console.log(response.data)
    setTableShowData(response.data);
  })
  },[])
  //console.log(tableShowData,"tableShowData")
  return (

      <React.Fragment>
        <Topbar toggleSidebar={toggleSidebar} pageTitle="User List" />
        <div className="my-1 px-4 lg:p-4">
        {!tableShowData && <LoadingModal/>}
        {tableShowData && <Table
            tableHeader={tableHeader}
            tableDataRow={tableShowData}
            tableAction={tableAction}
            rowLimit={tableShowData.per_page}
            tableFilter={true}
          />}
        </div>
    </React.Fragment>
  );
};

export default UserList;

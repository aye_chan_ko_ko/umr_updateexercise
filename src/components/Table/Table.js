import React, { useState, useEffect } from "react";
import TableFilter from "./TableFilter";
import TableHeader from "./TableHeader";
import TableDataRow from "./TableDataRow";
import TablePagination from "./TablePagination";
import axios from "axios";
import { TableActionContact } from "./TableActionContact";

const style = {
  backdropBlur: {
    backdropFilter: "blur(3px)",
    WebkitBackdropFilter: "blur(3px)",
    background: "rgba(255, 255, 255, 0.95)",
  },
};

const Table = ({
  rowLimit,
  tableDataRow,
  tableFilter,
  tableHeader,
  tableAction,
}) => {
  const [searchData, setSearchData] = useState("");
  const [searchTypeValue, setSearchTypeValue] = useState();
  const [paginationShow, setPaginationShow] = useState(true);
  const [currentMinRow, setCurrentMinRow] = useState(0);
  const [currentMaxRow, setCurrentMaxRow] = useState(rowLimit);
  const [resultData, setResultData] = useState(tableDataRow);
  const [isTableFilterOpen, setIsTableFilterOpen] = useState(false);
  const [line, setLine] = useState(1);
  const [currentPage, setCurrentPage] = useState();
  const [deactive, setDeactive] = useState(false);
  const [actionCheck, setActionCheck] = useState(false);

  // console.log(resultData,"show result data",tableDataRow.data)
  // console.log(currentMaxRow,"max",currentMinRow,"min")

  useEffect(() => {
    //console.log(currentPage,"three set dots")
    if (currentPage) {
      axios
        .get("https://api.picoehr.com/api/v1/users?page=" + `${currentPage}`)
        .then((res) => {
          setResultData(res.data);
          console.log(res, "useeffect resultData latettset");
        })
        .catch((err) => console.log(err));
    } else {
      if (!paginationShow) {
        axios
          .get(
            "https://api.picoehr.com/api/v1/users-search/" +
              `${searchTypeValue}/${searchData}`
          )
          .then((res) => {
            setResultData(res.data);
            setCurrentMaxRow(rowLimit);
            setCurrentMinRow(0);
            setPaginationShow(false);
            console.log(res.data);
          });
      }
    }
  }, [deactive]);

  const toggleTableFilter = () => {
    setIsTableFilterOpen(!isTableFilterOpen);
  };

  const handleSearchButton = (e) => {
    e.preventDefault();
    //console.log("search button")
    axios
      .get(
        "https://api.picoehr.com/api/v1/users-search/" +
          `${searchTypeValue}/${searchData}`
      )
      .then((res) => {
        //console.log(res,"search data");
        setResultData(res.data);
        setCurrentMaxRow(rowLimit);
        setCurrentMinRow(0);
        setPaginationShow(false);
      })
      .catch((err) => console.log(err, "errr search data"));
  };

  const loadNextData = () => {
    if (currentMaxRow < tableDataRow.total) {
      setCurrentMinRow(currentMaxRow);
      setCurrentMaxRow(currentMaxRow + rowLimit);
      setLine(line + 1);
      axios
        .get("https://api.picoehr.com/api/v1/users?page=" + `${line + 1}`)
        .then((res) => {
          // console.log(res.data.data,"kjfldfjlkfjdkfj");
          setResultData(res.data);
        })
        .catch((error) => console.log(error));
      //console.log(tableDataRow.next_page_url)
    }
  };
  //console.log(resultData)
  const loadPrevData = () => {
    if (currentMinRow > 0) {
      setCurrentMinRow(currentMinRow - rowLimit);
      setCurrentMaxRow(currentMinRow);
      setLine(line - 1);
      axios
        .get("https://api.picoehr.com/api/v1/users?page=" + `${line - 1}`)
        .then((res) => {
          // console.log(res.data.data,"kjfldfjlkfjdkfj");
          setResultData(res.data);
        })
        .catch((error) => console.log(error));
    }
  };
  const loadFistData = () => {
    if (currentMinRow >= 0) {
      setCurrentMinRow(0);
      setCurrentMaxRow(rowLimit);
      setLine(1);
      axios
        .get(tableDataRow.first_page_url)
        .then((res) => {
          // console.log(res.data.data,"kjfldfjlkfjdkfj");
          setResultData(res.data);
        })
        .catch((error) => console.log(error));
    }
  };
  const loadLastData = () => {
    if (currentMaxRow < tableDataRow.total) {
      setCurrentMinRow(tableDataRow.total - (tableDataRow.total % rowLimit));
      setCurrentMaxRow(tableDataRow.last_page * rowLimit);
      setLine(Math.ceil(tableDataRow.total / 10));
      //console.log(Math.ceil(tableDataRow.total/10))
      axios
        .get(tableDataRow.last_page_url)
        .then((res) => {
          // console.log(res.data.data,"kjfldfjlkfjdkfj");
          setResultData(res.data);
        })
        .catch((error) => console.log(error));
    }
  };
  const filteredData = (query) => {
    setResultData(
      tableDataRow.filter((tdr) =>
        tdr.reduce((prev, next) => (prev + next).toLowerCase()).includes(query)
      )
    );
  };
  //console.log(searchData ,"table searchData")
  const [tableToggle, setTableToggle] = useState(false);
  const [index1, setIndex1] = useState();
  //const tableToggle1 =[];
  // resultData.map(res=>{
  //   tableToggle1.push(false);
  // })
  // useEffect(()=>{
  //   if(index1){tableToggle1[index1]=true};
  //   console.log(tableToggle1,"tableToggle1",index1, "index",tableToggle)
  // },[index1])
  return (
    <div className="overflow-auto pb-10 text-sm md:border md:bg-white md:rounded-lg md:shadow-md md:overflow-visible md:pb-0">
      <div
        style={style.backdropBlur}
        className="fixed bottom-0 left-0 p-4 w-full border-t text-center lg:flex md:items-center md:p-1 md:justify-between md:static md:w-auto md:border-none z-20"
      >
        {tableFilter && (
          <div
            className={`-mt-2 mb-3 md:mb-0 md:mt-0 ${
              isTableFilterOpen ? null : "hidden md:block"
            }`}
          >
            <TableFilter
              filteredData={filteredData}
              searchData={searchData}
              setSearchData={setSearchData}
              searchTypeValue={searchTypeValue}
              setSearchTypeValue={setSearchTypeValue}
              handleSearchButton={handleSearchButton}
              loadFistData={loadFistData}
              setPaginationShow={setPaginationShow}
            />
          </div>
        )}
        {paginationShow && (
          <TablePagination
            tableFilter={tableFilter}
            toggleTableFilter={toggleTableFilter}
            isTableFilterOpen={isTableFilterOpen}
            loadNextData={loadNextData}
            loadPrevData={loadPrevData}
            loadFistData={loadFistData}
            loadLastData={loadLastData}
            rowCount={tableDataRow.total}
            minRow={currentMinRow}
            maxRow={currentMaxRow}
          />
        )}
      </div>
      <div className="block md:table w-full">
        <TableHeader
          tableHeader={tableHeader}
          tableAction={tableAction}
          setActionCheck={setActionCheck}
          actionCheck={actionCheck}
        />
        {resultData.data.map((row, index) => {
          const arr = [
            row.user_code,
            row.name,
            row.phone,
            row.department.name,
            row.township.name,
            row.is_active,
          ];
          //console.log(arr,"arrayarrayaray")
          {
            /* console.log(row.name ,row.user_code, row.phone ,row.is_active ,row.department.name,row.township.name) */
          }
          //console.log(actionCheck, "182");
          return (
            <React.Fragment key={index}>
              <TableActionContact.Provider
                value={{
                  setIndex1,
                  index1,
                  deactive,
                  setDeactive,
                  index,
                  tableToggle,
                  setTableToggle,
                  setCurrentPage,
                }}
              >
                <TableDataRow
                  tableHeader={tableHeader}
                  tableDataCol={arr}
                  rowCount={index + currentMinRow + 1}
                  tableAction={tableAction}
                  //index1 = {index1}
                  //  setIndex = {setIndex1}
                  //indexNo = {index}
                  resultData={resultData}
                  //tableToggle ={tableToggle}
                  //setTableToggle = {setTableToggle}
                  //setCurrentPage = {setCurrentPage}
                  //deactive = {deactive}
                  //setDeactive = {setDeactive}
                  actionCheck={actionCheck}
                  setActionCheck={setActionCheck}
                />
              </TableActionContact.Provider>
            </React.Fragment>
          );
        })}
      </div>
    </div>
  );
};

export default Table;
